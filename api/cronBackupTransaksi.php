<?php
    $connected = @fsockopen("www.google.com", 80); 
    if (!$connected){
        echo 'No internet connection '.date('Y-m-d H:i:s');
        exit(0);
    }
    fclose($connected);
    // connect to the mysql database
    if ($_SERVER['SERVER_NAME'] == 'localhost') {
        $link = mysqli_connect('localhost', 'root', '', 'db_pos');
    } else {
        $link = mysqli_connect('localhost', 'appstud1_root', 'Appstudio123', 'appstud1_pos');
    }
    if ($link->connect_errno) {
        echo "Failed to connect to MySQL: " . $link->connect_error;
        exit();
    }
    mysqli_set_charset($link,'utf8');
    
    $body = false;
    $transaksiList = mysqli_query($link, "SELECT * FROM tb_transaksi WHERE backup = 0");
    $no_faktur = "no_faktur = '0'";
    while($transaksi = $transaksiList->fetch_array(MYSQLI_ASSOC)) {
        $transaksiDetail = mysqli_query($link, "SELECT * FROM tb_detail_transaksi WHERE no_faktur = '{$transaksi['no_faktur']}'");
        while($detail = $transaksiDetail->fetch_array(MYSQLI_ASSOC)) {
            $transaksi['detail'][] = $detail;
        }
        $body[] = $transaksi;
        $no_faktur .= " OR no_faktur = '{$transaksi['no_faktur']}'";
    }
    if ($body) {
        $bodyJson = json_encode($body);
        $ch = curl_init('https://demo.appstudio.id/lampuweb-pos/api/backupTransaksi.php');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($bodyJson),
            'Authorization: Basic ' . base64_encode('admin:admin'))
        );

        $result = curl_exec($ch);
        $result = json_decode($result);
        if ($result->success) {
            mysqli_query($link, "UPDATE tb_transaksi SET backup = 1 WHERE $no_faktur");
        }
        echo $result->message.' '.date('Y-m-d H:i:s');
    } else {
        echo 'There is no new data '.date('Y-m-d H:i:s');
    }
    exit(0);
?>