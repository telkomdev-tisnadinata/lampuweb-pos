<div class="col-md-8 col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Produk</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 3%">#</th>
                <th style="width: 5%">ID Produk</th>
                <th style="width: 15%">Kode Produk</th>
                <th style="width: 20%">Nama Produk</th>
                <th style="width: 15%;">Harga per pcs</th>
                <th style="width: 15%;">Stok Tersedia</th>
            </tr>
            <?php
                $no = 1;
                $produkList = $modelProduk->get_produk_list();
                while($produk = $produkList->fetch_object()) {
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $produk->id_produk; ?></td>
                        <td><?php echo $produk->kode_produk; ?></td>
                        <td><?php echo $produk->nama_produk; ?></td>
                        <td><?php echo $modelApp->convert_to_rupiah($produk->harga_produk); ?></td>
                        <td>Tidak Terbatas</td>
                    </tr>
                <?php
                }
            ?>
            </tbody>
            </table>
        </div>
    </div>
</div>
