<div class="col-md-8 col-xs-12">
<?php
    if(isset($_POST['add'])) :
        $status = $modelTransaksi->add($_POST['kode_produk'], $_POST['qty']);
    endif;
    if(isset($_POST['update'])) :
        $modelTransaksi->update($_POST['kode_produk'], $_POST['qty']);
    endif;
    if(isset($_POST['delete'])) :
        $modelTransaksi->delete($_POST['kode_produk']);
    endif;
    if(isset($_GET['batal'])) :
        $modelTransaksi->destroy();
        echo '<meta http-equiv="refresh" content="0; url=?menu=transaksi_create">';
    endif;
    if(isset($_POST['buat_transaksi'])) :
        $result = $modelTransaksi->buat_transaksi($_POST);
        $status = ($result) ? 'success' : 'danger';
        $message = ($result) ? "Transaksi berhasil dibuat dengan nomor faktur $result" : 'Gagal Membuat Transaksi';
        echo "
        <div class='alert alert-$status alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
            <h4><i class='icon fa fa-info'></i> Pemberitahuan!</h4>
            $message
        </div>
        ";
    endif;
?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Pilih Produk</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="" method="post">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Kode Produk / Nama Produk / Harga per pcs</label>
                        <select class="select2" style="width: 100%;" name="kode_produk" required>
                        <?php
                            $produkList = $modelProduk->get_produk_list();
                            while($produk = $produkList->fetch_object()) {
                                echo "<option value='$produk->kode_produk'>$produk->kode_produk / $produk->nama_produk / ".$modelApp->convert_to_rupiah($produk->harga_produk)." </option>";
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Qty</label>
                        <input type="number" class="form-control" name="qty" value="1" min="0" placeholder="0" >
                    </div>
                    <!-- /.form-group -->
                </div>
                <div class="col-md-2">
                    <div class="form-group pull-right">
                        <label>&nbsp</label>
                        <input type="submit" class="form-control btn btn-primary btn-flat" name="add" value="TAMBAH">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>
            </form>
            <!-- /.row -->
        </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Barang Belanjaan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 5%">#</th>
                <th style="width: 14%">Kode Produk</th>
                <th style="width: 30%">Nama Produk</th>
                <th style="width: 13%;">Harga per pcs</th>
                <th style="width: 10%">Qty</th>
                <th style="width: 13%">Total Harga</th>
                <th style="width: 15%"></th>
            </tr>
            <?php
                $no=1;
                $subtotal = 0;
                $total = 0;
                $diskon = 0;
                if(!empty($_SESSION['produk'])) {
                    foreach($_SESSION['produk'] as $items => $row) { 
                        $subtotal = $row['harga_produk'] * $row['qty'];
                        $total += $subtotal;
                ?>
                    <form action="" method="post">
                    <tr>
                        <input type="hidden" name="kode_produk" value="<?php echo $row['kode_produk']; ?>">
                        <input type="hidden" name="harga_produk" value="<?php echo $row['harga_produk']; ?>">
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row['kode_produk']; ?></td>
                        <td><?php echo $row['nama_produk']; ?></td>
                        <td style="text-align: right;"><?php echo $modelApp->convert_to_rupiah($row['harga_produk']); ?></td>
                        <td>
                            <input type="number" class="form-control input-sm" name="qty" value="<?php echo $row['qty']; ?>" min="0" placeholder="0" >
                        </td>
                        <td style="text-align: right;" ><?php echo $modelApp->convert_to_rupiah($subtotal); ?></td>
                        <td align="center">
                            <input type="submit" class="btn btn-info btn-sm btn-flat" name="update" value="UBAH"/>
                            <input type="submit" class="btn btn-warning btn-sm btn-flat" name="delete" value="HAPUS"/>
                        </td>
                    </tr>
                    </form>
                <?php
                    }
                } else {
                    echo "<tr><td colspan='7'> Belum ada produk dipilih</td></tr>";
                }
            ?>
            </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-4 col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Detail Pembayaran</h3>
        </div>
        <!-- /.box-header -->
        <form action="" method="post">
            <div class="box-body">
                <table class="table table-striped">
                <tbody>
                <tr>
                    <td style="font-weight: bold;width: 70%">Total Belanja</td>
                    <td style="text-align: right;" id="total_belanja_text"><?php echo $modelApp->convert_to_rupiah($total); ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; color: red;">Diskon Belanja</td>
                    <td style="text-align: right; color: red;" id="diskon_belanja_text">- <?php echo $modelApp->convert_to_rupiah($diskon); ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 70%; color: green;">Total Tagihan</td>
                    <td style="text-align: right; font-weight: bold; color: green;" id="total_tagihan_text"><?php echo $modelApp->convert_to_rupiah($total-$diskon); ?></td>
                    <input type="hidden" id="total_tagihan" name="total_tagihan" value="<?php echo $total-$diskon; ?>" >
                </tr>
                <tr>
                    <td style="font-weight: bold;">Metode Pembayaran</td>
                    <td style="text-align: right;">Cash</td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Total Dibayar (Rp)</td>
                    <td>
                        <input type="number" class="form-control input-sm" id="total_dibayar" name="total_dibayar" onKeyUp="setTotalKembalian(this.value);" required>
                    </td>
                </tr>
                <tr>
                <td style="font-weight: bold; width: 70%; color: green;">Total Kembalian</td>
                    <td class="pull-right" style="font-weight: bold;" id="total_kembalian_text">Rp. 0</td>
                    <input type="hidden" id="total_kembalian" name="total_kembalian" value="0" >
                </tr>
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="?menu=transaksi_create&batal=1" class="btn btn-danger">BATALKAN TRANSAKSI</a>
                <input type="submit" class="btn btn-success pull-right" id="buat_transaksi" name="buat_transaksi" value="BUAT TRANSAKSI" disabled/>
            </div>
        </form>
    </div>
</div>
<script>
    function setTotalKembalian(total_dibayar) {
        var total_tagihan = parseInt(document.getElementById('total_tagihan').value);
        var total_kembalian = total_dibayar-total_tagihan;
        document.getElementById('total_kembalian').value = total_kembalian;
        document.getElementById('total_kembalian_text').innerHTML = convertToRupiah(total_kembalian);
        if (total_dibayar >= 0 && total_kembalian >= 0) {
            document.getElementById("buat_transaksi").removeAttribute("disabled");
        } else {
            document.getElementById("buat_transaksi").setAttribute("disabled", null);
        }
    }
</script>