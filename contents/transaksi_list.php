<div class="col-md-offset-1 col-md-10 col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Transaksi <small>Data akan di sinkronisasi dengan server utama setiap 10 detik.</small></h3>
            <button class="btn btn-primary btn-xs pull-right" style="margin-right: 1%;" onClick="location.reload();">MUAT ULANG HALAMAN</button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 3%">#</th>
                <th style="width: 15%">Merchant</th>
                <th style="width: 15%">Nomor Faktur</th>
                <th style="width: 15%">Tanggal Transaksi</th>
                <th style="width: 35%">Detail Transaksi</th>
                <th style="width: 10%;">Total Tagihan</th>
                <th style="width: 7%;">Sync</th>
            </tr>
            <?php
                $no = 1;
                $transaksiList = $modelTransaksi->get_transaksi_list();
                while($transaksi = $transaksiList->fetch_object()) {
                    $transaksiDetail = $modelTransaksi->get_produk_transaksi($transaksi->no_faktur);
                ?>
                    <form action="" method="post">
                    <input type="hidden" name="no_faktur" value="<?php echo $transaksi->no_faktur; ?>"/>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo "$transaksi->merchant<br>(by $transaksi->username)"; ?></td>
                        <td><?php echo $transaksi->no_faktur; ?></td>
                        <td><?php echo $transaksi->createdAt; ?></td>
                        <td><ul>
                        <?php
                            while($detail = $transaksiDetail->fetch_object()) {
                                echo "<li>{$detail->kode_produk}-{$detail->nama_produk} &nbsp | &nbsp Qty : {$detail->quantity} &nbsp | &nbsp Sub Total : {$modelApp->convert_to_rupiah($detail->subtotal)}</li>";
                            }
                        ?>
                        </ul></td>
                        <td align="right"><?php echo $modelApp->convert_to_rupiah($transaksi->total); ?></td>
                        <td align="center"><?php echo ($transaksi->backup) ? '<span class="label label-success">success</span>' : '<span class="label label-warning">pending</span>'; ?></td>
                    </tr>
                    </form>
                <?php
                }
            ?>
            </tbody>
            </table>
        </div>
    </div>
</div>
