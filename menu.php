<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
    <div class="inner">
        <h3>-</h3>

        <p>Create New Order</p>
    </div>
    <div class="icon">
        <i class="ion ion-bag"></i>
    </div>
    <a href="?menu=transaksi_create" class="small-box-footer"><strong>Create New Order</strong> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
    <div class="inner">
        <h3>
            <?php
                $transactionList = $modelTransaksi->get_transaksi_list();
                echo ($transactionList) ? $transactionList->num_rows : 'NaN';
            ?>
        </h3>

        <p>Transaction History</p>
    </div>
    <div class="icon">
        <i class="ion ion-stats-bars"></i>
    </div>
    <a href="?menu=transaksi_list" class="small-box-footer"><strong>Show All Transaction</strong> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
    <div class="inner">
    <h3>
            <?php
                $produkList = $modelProduk->get_produk_list();
                echo ($produkList) ? $produkList->num_rows : 'NaN';
            ?>
        </h3>
        <p>Data Produk</p>
    </div>
    <div class="icon">
        <i class="fa fa-dropbox"></i>
    </div>
    <a href="?menu=produk_list" class="small-box-footer"><strong>Show All Transaction</strong> <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<!-- ./col -->
