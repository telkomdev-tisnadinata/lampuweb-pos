<?php  
/**
 * Class Produk, 
 *
 * @package Produk
 * @author 
 **/
require 'App.php';

class Produk extends App
{
	public function __construct()
	{
		if(empty($_SESSION['is_login'])) :
			header("Location:login.php");
		endif;
	}

	/**
	 * Menampilkan Data Semua Produj
	 *
	 * @param String, Integer ( search, limit, offset) 
	 * @return Array
	 **/

	/**
	 * Menghitung Data Semua Produj
	 *
	 * @param String ( search ) 
	 * @return Integer
	 **/
	public function get_produk_list()
	{
		$query = mysqli_query($this->connection(), "SELECT * FROM tb_produk");

		return $query;
	}

	/**
	 * Menghitung Data produk detail
	 *
	 * @param Integer ( id_produk ) 
	 * @return Integer
	 **/
	public function get_produk($cari=0)
	{
		$query = mysqli_query($this->connection(), "SELECT * FROM tb_produk WHERE kode_produk LIKE '%{$cari}%' OR nama_produk LIKE '%{$cari}%'");

		return $query;
	}


	/**
	 * Menampilkan Data Produk
	 *
	 * @return Array
	 **/
	public function json()
	{
		$query = mysqli_query( $this->connection(), "SELECT * FROM tb_produk");
		return $query;
	}


} // END class 