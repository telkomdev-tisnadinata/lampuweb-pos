<?php  
/**
 * Class Transaksi
 *
 * @package transaksi
 * @author 
 **/

require 'Produk.php';

class Transaksi extends Produk
{

	function __construct()
	{

		if(empty($_SESSION['is_login'])) :
			header("Location:login.php");
		endif;
	}

	/**
	 * menyimpan Data Transaksi
	 *
	 * @return string
	 **/
	public function buat_transaksi($data)
	{
		$tanggal = date('Y-m-d');
		$no_faktur = 'POS'.date('YmdHis');

		$total = $_POST['total_tagihan'];
		$bayar = $_POST['total_dibayar'];
		$username = $_SESSION['user']['username'];
		$merchant = $this->get_app('company_name');
		$backup = ($_SERVER['SERVER_NAME'] == 'localhost') ? 0 : 1;
		$values = "'{$merchant}','{$username}','{$no_faktur}','{$tanggal}','{$bayar}', '{$total}', {$backup}";
		$sql = "INSERT INTO tb_transaksi (merchant, username, no_faktur, tanggal, bayar, total, backup) VALUES ($values)";
		$result = mysqli_query($this->connection(), $sql);
		if (!$result) {
			return $result;
		}
		foreach($_SESSION['produk'] as $items => $row) :
			$produk = mysqli_fetch_object($this->get_produk($row['kode_produk']));
			$subtotal = $row['harga_produk'] * $row['qty'];
			$values = "'{$no_faktur}','{$row['kode_produk']}','{$row['nama_produk']}','{$row['qty']}', '{$subtotal}'";
			mysqli_query($this->connection(), "INSERT INTO tb_detail_transaksi (no_faktur, kode_produk, nama_produk, quantity, subtotal) VALUES ({$values})");
		endforeach;
		$this->destroy();
		return $no_faktur;
	}


	/**
	 *  Menampilkan Data Transaksi
	 *
	 * @return Array
	 **/
	public function get_transaksi_list()
	{
		$query = mysqli_query($this->connection(), "SELECT * FROM tb_transaksi ORDER BY createdAt DESC");
		return $query; 
	}

	public function get_transaksi($keyword=0)
	{
		$query = mysqli_query($this->connection(), "SELECT * FROM tb_transaksi WHERE no_faktur = '{$keyword}'");
		return $query; 
	}

	/**
	 *  Menampilkan Data Transaksi
	 *
	 * @return Array
	 **/
	public function get_produk_transaksi($keyword=0)
	{
		$query = mysqli_query($this->connection(), "SELECT tb_detail_transaksi.*, tb_produk.* FROM tb_detail_transaksi INNER JOIN tb_produk ON tb_detail_transaksi.kode_produk = tb_produk.kode_produk WHERE tb_detail_transaksi.no_faktur = '{$keyword}'");
		return $query; 
	}
	public function add($kode_produk=0, $qty = 1)
	{
		$row = mysqli_fetch_object($this->get_produk($kode_produk));
		if(empty($_SESSION['produk'][$kode_produk])) :
			$_SESSION['produk'][$kode_produk] = array(
				'kode_produk' => $kode_produk,
				'qty' => $qty,
				'nama_produk' => $row->nama_produk,
				'harga_produk' => $row->harga_produk,
			);
		else :
			$cart = $_SESSION['produk'][$kode_produk];
			$_SESSION['produk'][$kode_produk] = array(
				'kode_produk' => $kode_produk,
				'qty' => $qty + $cart['qty'],
				'kode' => $row->kode_produk,
				'nama_produk' => $row->nama_produk,
				'harga_produk' => $row->harga_produk,
			);
		endif;
		return true;
	}

	/**
	 * Mengubah data Keranjang transaksi
	 *
	 * @param Integer ( kode_produk, quantity )
	 * @return string
	 **/
	public function update($kode_produk=0, $qty = 1)
	{
		$row = mysqli_fetch_object($this->get_produk($kode_produk));
		$cart = $_SESSION['produk'][$kode_produk];
		$_SESSION['produk'][$kode_produk] = array(
			'kode_produk' => $kode_produk,
			'qty' => $qty,
			'kode_produk' => $row->kode_produk,
			'nama_produk' => $row->nama_produk,
			'harga_produk' => $row->harga_produk,
		);
		return true;
	}

	/**
	 * Menghapu Keranjang transaksi
	 *
	 * @param Integer ( kode_produk_produk)
	 * @return string
	 **/
	public function delete($kode_produk=0)
	{
		unset($_SESSION['produk'][$kode_produk]);
		return true;
	}

	/**
	 * Menghapu Keranjang transaksi
	 *
	 * @param Integer ( kode_produk_produk)
	 * @return string
	 **/
	public function destroy()
	{
		foreach($_SESSION['produk'] as $items => $row) :
			unset($_SESSION['produk'][$row['kode_produk']]);
		endforeach;
		return true;
	}
} // END class Transaksi.php